#Values to be used during development. Here you might specify the URI of a database sitting on localhost. 
DEBUG = True # Turns on debugging features in Flask 
ENV = 'development' 
HOST = '0.0.0.0' # host in Flask 
PORT = 80