# app/views/index.py 
from flask import render_template, Blueprint 
from app import app 

lissajous_blueprint = Blueprint('lissajous', __name__) 

####### ROUTAGE INDEX ################ 
# Create the mappings from URLs 
@lissajous_blueprint.route('/lissajous', methods=['GET']) 
def lissajous(): 
    app.logger.info('Lissajous') 
    return render_template('lissajous.html') 
      
####### FIN ROUTAGE INDEX ############# 