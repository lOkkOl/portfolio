# app/views/index.py 
from flask import render_template, Blueprint 
from app import app 

guess_blueprint = Blueprint('guess', __name__) 

####### ROUTAGE INDEX ################ 
# Create the mappings from URLs 
@guess_blueprint.route('/guess', methods=['GET']) 
def guess(): 
    app.logger.info('Guess') 
    return render_template('guess.html') 
      
####### FIN ROUTAGE INDEX ############# 