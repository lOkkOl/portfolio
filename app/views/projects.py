# app/views/index.py 
from flask import render_template, Blueprint 
from app import app 

projects_blueprint = Blueprint('projects', __name__) 

####### ROUTAGE INDEX ################ 
# Create the mappings from URLs 
@projects_blueprint.route('/projects', methods=['GET']) 
def projects(): 
    app.logger.info('Projects') 
    return render_template('projects.html') 
      
####### FIN ROUTAGE INDEX ############# 