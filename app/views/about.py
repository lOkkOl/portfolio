# app/views/index.py 
from flask import render_template, Blueprint 
from app import app 

about_blueprint = Blueprint('about', __name__) 

####### ROUTAGE INDEX ################ 
# Create the mappings from URLs 
@about_blueprint.route('/about', methods=['GET']) 
def about(): 
    app.logger.info('About') 
    return render_template('about.html') 
      
####### FIN ROUTAGE INDEX ############# 