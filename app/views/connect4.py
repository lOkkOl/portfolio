# app/views/index.py 
from flask import render_template, Blueprint 
from app import app 

connect4_blueprint = Blueprint('connect4', __name__) 

####### ROUTAGE INDEX ################ 
# Create the mappings from URLs 
@connect4_blueprint.route('/connect4', methods=['GET']) 
def connect4(): 
    app.logger.info('Connect4') 
    return render_template('connect4.html') 
      
####### FIN ROUTAGE INDEX ############# 