# app/views/index.py 
from flask import render_template, Blueprint 
from app import app 

meteo_blueprint = Blueprint('meteo', __name__) 

####### ROUTAGE INDEX ################ 
# Create the mappings from URLs 
@meteo_blueprint.route('/meteo', methods=['GET']) 
def meteo(): 
    app.logger.info('Meteo') 
    return render_template('meteo.html') 
      
####### FIN ROUTAGE INDEX ############# 