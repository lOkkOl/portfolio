# app/views/index.py 
from flask import render_template, Blueprint 
from app import app 

index_blueprint = Blueprint('index', __name__) 

####### ROUTAGE INDEX ################ 
# Create the mappings from URLs 
@index_blueprint.route('/', methods=['GET']) 
def index(): 
    app.logger.info('Home page') 
    return render_template('index.html') 
      
####### FIN ROUTAGE INDEX ############# 