#!venv/bin/python 
# Import app variable from app_agelab package 
from app import app 

app.run(debug=True, host='0.0.0.0', port=8080) 
